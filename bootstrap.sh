#!/bin/bash

GREEN='\033[1;32m'
NC='\033[0m' # No Color

# This function is called at the very bottom of the file
main() {
    echo -e "${GREEN}## REALIZANDO UPDATE ##${NC}"
    update

    echo -e "${GREEN}## INSTALANDO FERRAMENTAS ##${NC}"
    tools

    echo -e "${GREEN}## AJUSTANDO O TIMEZONE ##${NC}"
    timezone

    echo -e "${GREEN}## APLICANDO O AUTOREMOVE ##${NC}"
    autoremove

    ln -s /vagrant/installer.sh /home/vagrant/
}
update() {
    # Update the server
    apt update
}

tools() {
    # Install basic tools
    apt -y install build-essential binutils-doc git unzip rsync
}

timezone(){
    # Set the Server Timezone to CST
    dpkg-reconfigure -f noninteractive tzdata
    timedatectl set-timezone America/Sao_Paulo
}

autoremove() {
    apt -y autoremove
}

main
exit 0