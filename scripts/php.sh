php_install() {
    echo -e "${GREEN}## INSTALANDO PHP ##${NC}"
    apt-get -y install php7.2-fpm php-pear php7.2-intl php7.2-mysql php7.2-sqlite php7.2-curl php7.2-dev php7.2-gd php7.2-mbstring php7.2-zip php7.2-xml
}