php_unit_install() {
    echo -e "${GREEN}## INSTALANDO PHP UNIT ##${NC}"
    # Install PHP Unit 4.8 globally
    if [ ! -f "/usr/local/bin/phpunit" ];
    then
        curl -O -L https://phar.phpunit.de/phpunit-old.phar
        chmod +x phpunit-old.phar
        mv phpunit-old.phar /usr/local/bin/phpunit
    fi
}