php_xdebug_install() {
    echo -e "${GREEN}## INSTALANDO XDEBUG ##${NC}"
    xdebug_config_file="/etc/php/7.2/mods-available/xdebug.ini"
    apt-get -y install php7.2-xdebug

        cat << EOF > ${xdebug_config_file}
zend_extension=xdebug.so
xdebug.remote_enable=1
xdebug.remote_connect_back=1
xdebug.remote_port=9000
xdebug.remote_host=10.0.2.2
EOF
    service php7.2-fpm restart
}