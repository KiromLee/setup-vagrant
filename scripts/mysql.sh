mysql_menu(){
    echo -e "\n${GREEN}## ESCOLHA UMA DAS OPÇÕES ABAIXO ##${NC}\n"
    echo -e "1) Instalar MYSQL"
    echo -e "2) Criar banco de dados"
    echo -e "9) Voltar"
    echo -e "0) Sair\n"
    read option
    case $option in
        1) mysql_install ;;
        2) mysql_create_database ;;
        9) menu ;;
        0) exit 0 ;;
        *) echo -e "Opcao Invalida!" ;;
    esac
}

mysql_create_database(){
    echo -e "Digite o nome do banco!"
    read database_name
    if [ $MYSQL_ROOT_PASS ]
    then
        pass_root=$MYSQL_ROOT_PASS
    else
        read pass_root
    fi
    echo -e "Criando banco de dados!"
    echo "CREATE DATABASE ${database_name}" | mysql -uroot -p$pass_root
    echo -e "Subindo arquivo sql"
    zcat /vagrant/databases/${database_name}.sql.gz | mysql -u 'root' -p$pass_root $database_name
}

mysql_install(){
    # Install MySQL
    echo -e "${GREEN}## INSTALANDO MYSQL ##${NC}"

    rm /vagrant/mysql/empty

    apt -y install mariadb-client mariadb-server

    ##Allow remote acess
    sed -i 's/127\.0\.0\.1/0\.0\.0\.0/g' /etc/mysql/mariadb.conf.d/50-server.cnf
    
    mysql_sync_backup
}

mysql_sync_backup (){
    service mysql stop
    if [ $MYSQL_PATH ]
    then
        if [ "$(ls ${MYSQL_PATH} | wc -l)" == 0 ];
        then
            rsync -Craz /var/lib/mysql/* $MYSQL_PATH
        else
            rm -rf /var/lib/mysql/*
            rsync -Craz $MYSQL_PATH* /var/lib/mysql/
            rsync -Craz /var/lib/mysql/* $MYSQL_PATH
        fi
        echo "* * * * * /bin/bash /vagrant/mysql_sync.sh" >> /var/spool/cron/crontabs/vagrant
        sudo chmod 600 /var/spool/cron/crontabs/vagrant
        service cron reload
    fi
    service mysql start
}