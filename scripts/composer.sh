composer_install() {
    echo -e "${GREEN}## INSTALANDO COMPOSER ##${NC}"
    # Install latest version of Composer globally
    if [ ! -f "/usr/local/bin/composer" ];
    then
        curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
    fi
}