nginx_install() {
    echo -e "${GREEN}## INSTALANDO NGINX ##${NC}"
    apt-get -y install nginx
    mv /etc/nginx/sites-available/default /etc/nginx/sites-available/default.bak
    rm /etc/nginx/sites-enabled/default
    ln -s /vagrant/nginx/enabled/* /etc/nginx/sites-enabled/
    systemctl enable nginx
    service nginx restart
}