redis_install(){
    echo -e "${GREEN}## INSTALANDO REDIS ##${NC}"
    apt-get -y install redis php7.2-redis
    service php7.2-fpm restart
}