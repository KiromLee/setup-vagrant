#!/bin/bash

GREEN='\033[1;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

export $(grep -v '^#' /vagrant/.env | xargs -d '\n')

. /vagrant/scripts/mysql.sh
. /vagrant/scripts/nginx.sh
. /vagrant/scripts/redis.sh
. /vagrant/scripts/php.sh
. /vagrant/scripts/xdebug.sh
. /vagrant/scripts/php_unit.sh
. /vagrant/scripts/composer.sh

main() {
    user=`whoami`
    if [ $user != "root" ]
    then
        echo -e "\n${RED}## ESTE SCRIPT DEVE SER CHAMADO COM PERMISSÕES DE ROOT ##${NC}\n"
        exit 0
    fi

    while [  true ]; do
        menu
    done
}

menu(){
    echo -e "\n${GREEN}## ESCOLHA UMA DAS OPÇÕES ABAIXO ##${NC}\n"
    echo -e "1) INSTALAÇÃO COMPLETA"
    echo -e "2) Instalar MYSQL"
    echo -e "3) Instalar NGINX"
    echo -e "4) Instalar REDIS"
    echo -e "5) Instalar PHP"
    echo -e "6) Instalar XDEBUG"
    echo -e "7) Instalar PHP UNIT"
    echo -e "8) Instalar COMPOSER"
    echo -e "9) Menu MYSQL"
    echo -e "0) Sair\n"
    read option
    case $option in
        1) instalacao_completa ;;
        2) mysql_install ;;
        3) nginx_install ;;
        4) redis_install ;;
        5) php_install ;;
        6) xdebug_install ;;
        7) php_unit_install ;;
        8) composer_install ;;
        9) mysql_menu ;;
        0) exit 0 ;;
        *) echo -e "Opcao Invalida!" ;;
    esac
}

instalacao_completa(){
    mysql_install
    nginx_install
    redis_install
    php_install
    xdebug_install
    php_unit_install
    composer_install
    autoremove
}

autoremove() {
    apt -y autoremove
}

main
exit 0